import React, { Component } from 'react';
import { connect } from 'react-redux';
import Fullpage, { FullPageSections, FullpageSection } from '@ap.cx/react-fullpage';
import { fetchGamesAction} from "./actions/app";
import FirstSection from './components/board-game-page/first-section';
import SecondSection from './components/board-game-page/second-section';
import ThirdSection from "./components/board-game-page/third-section";
import mapDispatchToProps from "react-redux/lib/connect/mapDispatchToProps";

class App extends Component {
    componentDidMount() {
        this.props.fetchGamesAction();
    }

    render () {
        if (this.props.game) {
            return (
                <Fullpage>
                    <FullPageSections>

                        <FullpageSection>
                            <FirstSection game={this.props.game}/>
                        </FullpageSection>

                        <FullpageSection>
                            <SecondSection game={this.props.game}/>
                        </FullpageSection>

                        <FullpageSection>
                            <ThirdSection game={this.props.game}/>
                            {this.props.comments.map((c) => {
                                return <div>{c.comment}</div>
                            })
                            }
                        </FullpageSection>
                    </FullPageSections>
                </Fullpage>
            )
        }

        if (this.props.isFetching)
            return (
                <div>Loading...</div>
            )
        return null;
    }
}

function MapStateToProps(state) {
    console.log(state)
    return{
        comments: state.app.comments,
        game: state.app.games[0],
        isFetching: state.app.isFetching
    };
}

function MapDispatchToProps() {
    return{
        fetchGamesAction
    };
}

export default connect(MapStateToProps, MapDispatchToProps())(App)
