import React from 'react';
import "./third-section.css"
import { connect } from 'react-redux';
import { addCommentAction,fetchCommentsAction,deleteCommentAction } from "../../../actions/app";

class ThirdSection extends React.Component{
    state ={
        name:"",
        comment:""
    };

    render() {
        return(
            <div className={'third-section'}>
                <div className={"third-section__content-wrapper"}>
                      <form className={"third-section__form"}>
                          <label>Имя:</label>
                              <div className={"third-section__form-group"}>
                              <input
                                  onChange={this.handleNameChange}
                                  className={"third-section__name-input"}
                                  value={this.state.name}
                                  type="text"/>
                          </div>
                          <div className={"third-section__form-group"}>
                              <label>Коммент:</label>
                              <textarea
                                  onChange={this.handleCommentChange}
                                  className={"third-section__comment-area"}
                                    value={this.state.comment}/>
                          </div>
                          <div className={"third-section__form-group"}>
                              <button
                                  onClick={this.handleButtonClick}
                                  className={"third-section__button"}>
                                  Отправить
                              </button>
                          </div>
                      </form>
                    <div className={"third-section__comments-wrapper"}>
                    {this.renderComments()}
                    </div>
                 </div>
            </div>
        );
    }
    renderComments() {
        const {comments} = this.props;
        return (
            comments.map((comment) => {
                return (
                    <div className={"third-section__comment-wrapper"}>
                        <h6>{comment.name}</h6>
                        <p>{comment.text}</p>
                        <button onClick={
                            this.handleCommentDelete.bind(this, comment._id)
                        }>X</button>
                    </div>
                )
            })
        )
    }

    componentDidMount() {
        this.props.fetchComments();
    }

    handleCommentDelete(id){
        this.props.deleteComment(id);
    }
    handleNameChange = (e) =>{
        this.setState({name : e.target.value});
    };
    handleCommentChange = (e) =>{
        this.setState({comment : e.target.value});
    };
    handleButtonClick = (e) =>{
        e.preventDefault();

        this.props.addComment(this.state.name, this.state.comment);
        this.setState({
            name:'',
            comment:''
        });
    }

}
function MapStateToProps(state) {

    const  game = state.app.games[0];
    return{
        comments: state.app.comments.filter((comment) => {
           return comment.game_id == game._id;
    })
    }
}
function MapDispatchToProps() {
    return{
        addComment: addCommentAction,
        fetchComments: fetchCommentsAction,
        deleteComment: deleteCommentAction
    }
}
export default connect(MapStateToProps, MapDispatchToProps())(ThirdSection);