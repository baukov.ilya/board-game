import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCoffee } from "@fortawesome/free-solid-svg-icons";
import './first-section.css';
import {icon} from "@fortawesome/fontawesome-svg-core";

class FirstSection extends React.Component {
    render() {
        const { game } = this.props;
        return (
            <div className='first-section'>

                <div className='first-section__content-wrapper'>
                    <h1 className='first-section__game-heading'>{game.name}</h1>
                    <p className='first-section__game-description'>{game.title_description}</p>
                <div className="first-section__icon-wrapper">
                    {
                        game.icons.map((icon) => {
                            return (
                                <img src={ icon } alt="" className="first-section__icon"/>
                            )
                        })
                    }
                </div>
                    <button className="first-section__buy-button">Купить</button>
                </div>
            </div>
        )
    }
}

export default FirstSection;
