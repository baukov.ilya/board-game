const initialState = {
    comments:[],
    games:[],
    isFetching: false
};

function appReducer(state = initialState, action) {
    console.log(action);
    console.log(state);
    switch(action.type){
        case 'ADD_COMMENT':
            return {
                ...state,
                comments: [
                    ...state.comments,{
                        name: action.name,
                        text: action.text,
                        game_id: action.game_id,
                        _id: action._id
                    }
                ]
            };
        case 'FETCH_GAMES_SUCCESS':
        return {
            ...state,
            games: action.games,
            isFetching: false
        };
        case "FETCH_GAMES":
            return{
                ...state,
                isFetching: true
            };
        case "FETCH_COMMENTS":
            return {
                ...state,
                isFetchingComments: false
            };
        case "FETCH_COMMENTS_SUCCESS":
            return {
                ...state,
                comments: action.comments,
                isFetchingComments:true
            };
        case "DELETE_COMMENT":
            return{
                ...state,
                comments: state.comments.filter((comment) =>{
                    return comment._id !== action.comment_id;
                })
            };
        default:
            return state;
    }
}
export default appReducer;