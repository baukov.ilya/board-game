
export function addCommentAction(name, comment) {
    return async (dispatch, getState) => {
        const state = getState();
        const game = state.app.games[0];

        const response = await fetch("http://localhost:3012/comments", {
            method: "POST",
            headers:{
                'Content-Type':'application/json'
            },
            body: JSON.stringify({
                name: name,
                text: comment,
                game_id: game._id
            })
        });
        const DBcomment = await response.json();
        if (response.status == 200) {
            dispatch({
                type: 'ADD_COMMENT',
                name: DBcomment.name,
                text: DBcomment.text,
                game_id: game._id,
                _id: DBcomment._id
            });
        }
    }

};

export function deleteCommentAction(comment_id) {
    return async (dispatch) => {
        const response = await fetch(`http://localhost:3012/comments/${comment_id}`, {
            method: "DELETE"
        });
        if (response.status == 200) {
            dispatch({
                type: 'DELETE_COMMENT',
                comment_id
            });
        }
    };
}



export function fetchGamesAction() {
    return async(dispatch, getState) => {

        dispatch({
            type:"FETCH_GAMES"
        });


        const response = await fetch("http://localhost:3012/boarder_games");
        const games = await response.json();
        console.log(games);

        dispatch({
            type: 'FETCH_GAMES_SUCCESS',
            games
        });

        console.log(getState());
    }
}

export function fetchCommentsAction() {
    return async(dispatch, getState) => {
        dispatch({
            type:"FETCH_COMMENTS"
        });


        const response = await fetch("http://localhost:3012/comments");
        const comments = await response.json();

        dispatch({
            type: 'FETCH_COMMENTS_SUCCESS',
            comments
        });

    }
}
